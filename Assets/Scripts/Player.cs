﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.InputSystem;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{
    public PlayerInput controls;
    
    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private float minSpeed;
    [SerializeField]
    private float acceleration;
    [SerializeField]
    private float deceleration;
    [SerializeField]
    private float speed;

    private SpriteRenderer sprite;
    private Rigidbody2D rigidBody;


    private bool isRunning = false;
    private bool isMoving = false;

    private Collider2D touchingObject;

    private void Awake()
    {
        controls = GetComponent<PlayerInput>();
        Assert.IsNotNull(controls.currentControlScheme);
        controls.actions["Run"].performed += cntx => 
        {
            if (cntx.ReadValueAsButton()){
                //Debug.Log(rigidBody.velocity.sqrMagnitude);
                if (isMoving) StartRunning();
            }
            else StopRunning();
        };
        controls.actions["Use"].performed += _ => Use(touchingObject);

        sprite = GetComponentInChildren<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {        
        var movement = controls.actions["Movement"].ReadValue<Vector2>();
        Move(movement);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        touchingObject = collider;
    }
    
    private void OnTriggerExit2D(Collider2D collider)
    {
        touchingObject = null;
    }

    private void OnEnabled()
    {
        controls.actions["Run"].Enable();
        controls.actions["Movement"].Enable();

    }

    private void OnDisable() 
    {
        controls.actions["Run"].Disable();
        controls.actions["Movement"].Disable();
    }

    private void Move(Vector2 direction)
    {
        isMoving = direction.sqrMagnitude > 0;
        //Debug.Log("Wake up, Neo... You obosralsya" + direction);
        if(isRunning) {
            if (speed < maxSpeed) speed += acceleration;
        } else {
            speed -= deceleration;
            if (speed < minSpeed) {
                speed = minSpeed;
            }
        }
        rigidBody.MovePosition(rigidBody.position + direction * speed);
    }

    private void Use(Collider2D collider)
    {
        if (collider != null)
        {
            var touchingObject = collider.gameObject;
            var usable = touchingObject.GetComponent<IUseable>();
            if (usable != null)
            {
                Debug.Log("touching Useable");
                usable.OnUsed();
            }
        }
    }

    private void StartRunning()
    {
        Debug.Log("Run, bitch! RUUUUUUUN!!!");
        isRunning = true;
    }

    private void StopRunning()
    {
        // Debug.Log("Stop, bitch! STOOOOOP!!!");
        isRunning = false;
}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Collider2D))]
public class TeleportArea : MonoBehaviour
{
    public Transform dest;
    
    private void Start()
    {
        var collider = GetComponent<Collider2D>();
        Assert.IsTrue(collider.isTrigger);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Assert.IsNotNull(dest);
        if (other.gameObject.CompareTag("Player"))
        {
            var player = other.gameObject;
            var shift = transform.position - player.transform.position;
            player.transform.position = dest.position - shift;
        }
    }
}

﻿using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Character
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Animator))]
    public class CharacterBehaviour : MonoBehaviour
    {
        public float Acceleration = 5.0f;
        public float Deceleration = 5.0f;
        public float MinSpeed = 5.0f;
        public float MaxSpeed = 10.0f;

        private void Awake()
        {
            input = GetComponent<PlayerInput>();
            rigidbody = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();

            input.actions["Run"].started += _ => StartRunning();
            input.actions["Run"].performed += ctx =>
            {
                if (!ctx.ReadValueAsButton())
                {
                    StopRunning();
                }
            };
        }

        private void FixedUpdate()
        {
            var direction = input.actions["Move"].ReadValue<Vector2>();
            if (direction.sqrMagnitude > 0 && (speed < MinSpeed || isRunning))
            {
                speed += Acceleration * Time.fixedDeltaTime;
            }
            else
            {
                speed -= Deceleration * Time.fixedDeltaTime;
            }

            var scale = transform.localScale;
            if (direction.x > 0 && transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
            }
            else if (direction.x < 0 && transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
            }
            
            speed = Mathf.Clamp(speed, 0, MaxSpeed);
            var movement = direction * (speed * Time.fixedDeltaTime);
            rigidbody.AddForce(movement);
            var SpeedParamValue = speed / MaxSpeed; // normalize speed
            animator.SetFloat(SpeedParam, SpeedParamValue);
        }

        private void StartRunning()
        {
            Debug.Log("Start Run");
            isRunning = true;
        }

        private void StopRunning()
        {
            Debug.Log("Stop Run");
            isRunning = false;
        }

        [NotNull] private PlayerInput input;
        [NotNull] private new Rigidbody2D rigidbody;
        [NotNull] private Animator animator;
        private static readonly int SpeedParam = Animator.StringToHash("Speed");
        private float speed;
        private bool isRunning;
    }
}
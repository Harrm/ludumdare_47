﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class EyesBehaviour : MonoBehaviour
{
    public float EyesightRange = 30.0f;
    public float Speed = 10.0f;
    public float PushForce = 10.0f;
    
    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        Assert.IsNotNull(player);
    }

    private void Update()
    {
        Debug.DrawLine(transform.position, player.transform.position, Color.red);
    }

    private void FixedUpdate()
    {
        var distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
        if (distanceToPlayer < EyesightRange)
        {
            Vector2 directionTo =  player.gameObject.transform.position - transform.position;
            directionTo.Normalize();
            rigidbody.MovePosition(rigidbody.position + directionTo * Vector2.right * (Speed * Time.fixedDeltaTime));
            animator.SetFloat(SpeedParam, 1.0f);
        }
        else
        {
            animator.SetFloat(SpeedParam, 0.0f);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var directionAway = other.gameObject.transform.position - transform.position ;
            Debug.DrawLine(other.gameObject.transform.position, -directionAway, Color.cyan, 1.0f);
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(directionAway * PushForce, ForceMode2D.Force);
        }
    }

    private GameObject player;
    private new Rigidbody2D rigidbody;
    private Animator animator;
    private static readonly int SpeedParam = Animator.StringToHash("Speed");
}
